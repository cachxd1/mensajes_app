/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cesar1e.mensajes_app;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author Cesar 1e
 */
public class Inicio {
    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
        int opcion = 0;
        do{
            System.out.println("--------------------");
            System.out.println("Aplicación de mensajes");
            System.out.println("1. Crear un mensaje");
            System.out.println("2. Listar mensajes");
            System.out.println("3. Eliminar un mensaje");
            System.out.println("4. Editar un mensaje");
            System.out.println("5. Salir");

            //Leemos la opción del usuario
            opcion = sc.nextInt();
            
            switch(opcion){
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.borrarMensaje();
                    break;
                case 4:
                    MensajesService.editarMensaje();
                    break;
                default:
                    break;
            }
        }while(opcion != 5);
    }
}
